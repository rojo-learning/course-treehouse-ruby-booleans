
# Treehouse - Ruby Booleans #

This repository contains notes and practice examples from the course
**Ruby Booleans**, imparted by Jason Seifer at
[Threehouse][JF].

> In Ruby Booleans, you're going to learn the ins and outs of working with
> boolean values in Ruby. You'll learn about using booleans -- true and false
> values -- in your programs, as well as precedence, nil, conditional
> assignment, and more.

## Contents ##

- **Ruby Booleans**: Boolean values are logical values that represent either
  true or false.
- **Build a Simple TODO List Program**: Build a simple todo list program.


---
This repository contains code examples from Treehouse. These are included under
fair use for showcasing purposes only. Those examples may have been modified to
fit my particular coding style.

[JF]: http://teamtreehouse.com
