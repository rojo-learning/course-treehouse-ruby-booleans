
# Ruby Booleans #

A boolean value is a value that can be true or not. When a value is not true, it
is called false.

## Negation

Negating statements with the not operator (!) turns values from true to false
and vice versa.

## True and False

`true` and `false` are object singletons that belong to the `TrueClass` and
`FalseClass`, respectively.

```ruby
  puts true               # => true
  puts true.class         # => TrueClass
  puts true.inspect       # => "true"
  puts true.to_s          # => "true"

  puts false              # => false
  puts false.class        # => FalseClass
  puts false.inspect      # => "false"
  puts false.to_s         # => "false"
```

## Nil

`nil` is the complete absence of anything. The `nil` object is the singleton of
the `NilClass`.

```ruby
  nil.class               # => NilClass
  nil.to_i                # => 0
  nil.to_a                # => []
  nil.to_h                # => {}
  nil.nil?                # => true
```

## Precedence

Precedence refers to the order in which statements are evaluated in a program.
The use of `()` can alter the default precedence, making the programs to behave
differently.

```ruby
  1 && (2 == 1) && 2     # => false
  1 && 2 == (1 && 2)     # => true
```

## Conditional Assignment

The conditional assignment is a Ruby idiom that will assign a value to a
variable if that variable is not already set.

```ruby
  name ||= 'David'
  name                   # => 'David'
  name ||= 'Jason'
  name                   # => 'David'
```
