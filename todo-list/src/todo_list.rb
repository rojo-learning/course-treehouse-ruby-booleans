
require_relative 'todo_item'

class TodoList
  attr_reader :name, :todo_items

  def initialize(name)
    @name = name
    @todo_items = []
  end

  def add_item(name)
    @todo_items.push TodoItem.new(name)
  end

  def remove_item(name)
    @todo_items.delete_if { |item| item.name.eql? name }
  end

  def mark_complete(name)
    @todo_items[find_index name].mark_complete!
  end

  def find_index(name)
    @todo_items.find_index { |item| item.name == name }
  end

  def print(kind='all')
    puts "#{name} List - #{kind} items"
    puts "-" * 30

    todo_items.each do |todo_item|
      case kind
      when 'all'
        puts todo_item
      when 'complete'
        puts todo_item if todo_item.complete?
      when 'incomplete'
        puts todo_item unless todo_item.complete?
      end
    end
    
    puts "\n"
  end
end
