
require_relative 'src/todo_list'

todo_list = TodoList.new('Groceries')

todo_list.add_item 'Milk'
todo_list.add_item 'Eggs'
todo_list.add_item 'Bread'

todo_list.remove_item 'Eggs'
todo_list.mark_complete 'Bread'

todo_list.print
todo_list.print 'complete'
todo_list.print 'incomplete'
